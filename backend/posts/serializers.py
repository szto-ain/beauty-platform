from rest_framework import serializers
from .models import Post


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        feilds = ('id', 'title', 'content', 'created_at', 'updated_at',)
        model = Post
