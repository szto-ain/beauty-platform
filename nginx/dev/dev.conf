user  nginx;
worker_processes  1;

events {
  worker_connections  1024;
}

http {
  include /etc/nginx/mime.types;
  client_max_body_size 100m;

  upstream docker-frontend {
    server frontend:8080;
  }

  server {
    resolver 127.0.0.11 valid=30s;
    listen 80;
    charset utf-8;

    # frontend urls
    location / {
        proxy_redirect off;
        proxy_pass http://docker-frontend;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
    }

    # frontend dev-server
    location /sockjs-node {
      proxy_redirect off;
      proxy_pass http://docker-frontend;
      proxy_set_header X-Real-IP  $remote_addr;
      proxy_set_header X-Forwarded-For $remote_addr;
      proxy_set_header Host $host;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
    }

    # backend urls
    location ~ ^/(admin|api) {
      proxy_redirect off;
      proxy_pass http://localhost:8000;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
    }

    #static block
    location /static {
      autoindex on;
      alias /usr/src/app/static;
    }
  }
}
